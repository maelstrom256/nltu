import ansi.colour.fg as fg
import ansi.colour.bg as bg
import pltable as plt
from pretty_dbg import pdbg

"""
Define wraps as a dict of lists
Every wrap formed as list of functions from ansi module to apply
Cuctom wraps might been taken via Reporter class constructor
"""
wraps_def = {
    'title': [fg.boldyellow],
    'matched': [fg.boldgreen],
    'nonmatched': [fg.brightred, bg.darkgray],
}

class Reporter(object):
    """
    Accumulate, build and prepare report output to console
    Output might be rendered to file, with corrections in wrap function
    or alternate realisation for HTML format
    CSV output with minor corrections is also possible
    """
    def __init__(self, wraps = None):
        super(Reporter, self).__init__()
        self.ptc = plt.PrettyTable()
        self.ptc.set_style(plt.UNICODE_LINES)
        self.wraps = wraps if wraps else wraps_def

    def wrap(self, text, style = None):
        """
        Wraps text with appropriate style from `self.wraps` property
        Returns text wrapped with ansi escape codes
        Always preconverts text value to `str`
        """
        text = str(text)
        if not style:
            return text
        if style and style in self.wraps:
            for st in self.wraps[style]:
                text = st(text)
            return text
        else:
            return f"Invalid style:|{style}|\n{text}"

    def report(self, test):
        """
        Creates a report from Curler-filled results
        Enables Unicode Boxing characters instead of ascii boxing

        """
        ### Prepare PrettyTable
        pt = plt.PrettyTable()
        pt.set_style(plt.UNICODE_LINES)
        pt.header = True
        pt.title = self.wrap(f"{test['url']}", style='title')
        pt.add_column('', ['count', 'matched'])
        ### Prepare total counter and matched counter
        t_count = 0
        t_matched = 0
        ### check expects and fill the table
        for e in test['expects']:
            ### count totals
            t_count += e['count']
            t_matched += e['matched']
            ### set style depending on matched expects
            style = 'matched' if e['count'] == e['matched'] else 'nonmatched'
            ### Form list of expects and matches and style them
            items = [e['count'], e['matched']]
            items = [self.wrap(item, style) for item in items]
            ### Add column to PrettyTable
            pt.add_column(self.wrap(e['name'], style), items)
        ### Set style for 'Totals' line
        style = 'matched' if t_count == t_matched else 'nonmatched'
        ### Call separate injecttion function for totals
        ppt = self.PtTotal(pt, 'Total:', f"{t_matched} of {t_count} matched", style)
        print(ppt)


    def PtTotal(self, pt, total_str, total_value, style=None):
        """
        This function parses PrettyTable lines and injects 'Totals' into it
        * Get list of strings by `split("\n")`
        * Set up common line symbol and substitutes and generate separator line before 'Totals'
        * Smooth up final line
        * Determine max text width
        * Form text line for 'Totals' and wrap it with style provided
        """
        ### Print table and return if call mistaken
        if not (total_str and total_value):
            print(pt)
            return
        ### Form line before 'Totals'
        pt_lines = pt.get_string().split("\n")
        ref_line = pt_lines.pop()
        pt_line_symbol = '─'
        pt_line_symbols = {
            '╚': '╟',
            '╝': '╢',
            '╧': '┴',
        }
        pt_total_sep_line = ''
        for s in ref_line:
            sym = pt_line_symbols[s] if s in pt_line_symbols else pt_line_symbol
            pt_total_sep_line += sym
        pt_lines.append(pt_total_sep_line)
        ### Form final line
        final_line = ''
        for s in ref_line:
            sym = '═' if s == '╧' else s
            final_line += sym
        pt_lines.append(final_line)
        ### From 'Totals' line
        max_text_width = len(ref_line) - 4
        pad_len = max_text_width - len(total_value)
        total_line = f'{total_str: <{pad_len}}{total_value}'
        if style:
            total_line = self.wrap(total_line, style)
        total_line = f'║ {total_line} ║'
        pt_lines.insert(-1, total_line)
        ### Join list of strings to one string and return
        return '\n'.join(pt_lines)

import pycurl
from io import BytesIO
from copy import deepcopy as dcp
from pretty_dbg import pdbg
from Reporter import Reporter
import re

### curl deprecation warning suppression
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

### Accepts (key, value) tuple and dict as input, returns nothing
###### if no key exists in dict will assign the value
###### If key exists — will extend it to a list and append given value
###### or replace existing list with their sum
def xtend_dict_val(x, d):
    (k, v) = x
    ###### assign if no [k], concatenate lists if already list, else convert to list
    d[k] = v if not k in d else d[k] + [v] if type(d[k]) is list else [d[k], v]

### Accepts two dicts, returns nothing
###### replaces t(arget)'s values vith s(ource) values
###### concatenate lists from both dicts, if found
def merge_dicts_lists(t, s):
    for k in s:
        try:
            ###### if [k] exist and list — concatenate lists, else do deepcopy
            t[k] = t[k] + s[k] if type(s[k]) is list and k in t.keys() else dcp(s[k])
        except:
            print(f"Failed to copy key`{k}` with value {s[k]}")
            raise

### Merge defaults and provided options into curl options 
def defaults2curl_opts(data):
    """
    Converts defaults to curl options
    """
    opts = data['curl_options']
    tgt = data['target']
    srv = data['server-type']
    opts['RESOLVE'] = data['targets'][tgt]['dns']
    host = data['targets'][tgt]['hostnames'][srv]
    opts['URL'] = f"{data['proto']}://{host}{data['path']}"

### Main class for serving test suits
class Curler(object):
    """
    Provides base for running `curl` queries against web-server
    Opts filled with
    """
    def __init__(self, suits):
        super(Curler, self).__init__()
        self.suits = suits
        self.Reporter = Reporter()
        
### Run tests
    def execute_tests(self):
        for s in self.suits['test-suits']:
            suit = self.suits['test-suits'][s]
            suit_params = dcp(self.suits['defaults'])
            merge_dicts_lists(suit_params, suit['defaults'])
            for t in suit['tests']:
                params = dcp(suit_params)
                merge_dicts_lists(params, suit['tests'][t])
                defaults2curl_opts(params)
                suit['tests'][t]['url'] = params['curl_options']['URL']
                params['results'] = []
                pdbg(params, desc="test case")
                suit['tests'][t]['results'] = self.execute_curl(params)
                suit['tests'][t]['results'] = check_expects(suit['tests'][t]['expects'], suit['tests'][t]['results'])
                self.Reporter.report(suit['tests'][t])

### Execute single test case
    def execute_curl(self, test):
        c = pycurl.Curl()
        for c_opt in test['curl_options']:
            opt = getattr(pycurl, c_opt)
            val = test['curl_options'][c_opt]
            pdbg(val, c_opt)
            c.setopt(opt, val)
        cnt = test['repeat']
        results = []
        while cnt:
            ### Assign new buffers for response headers and body
            respHeadersBin = BytesIO()
            respBodyBin = BytesIO()
            c.setopt(pycurl.WRITEHEADER, respHeadersBin)
            c.setopt(pycurl.WRITEDATA, respBodyBin)
            ### Perform request and parse the data
            c.perform()
            resp = parse_response(respHeadersBin, respBodyBin)
            ### Fill query timings
            resp['Timings'] = {}
            resp['Timings']['total'] = f"{c.getinfo(pycurl.TOTAL_TIME):.3f}"
            resp['Timings']['nslookup'] = f"{c.getinfo(pycurl.NAMELOOKUP_TIME):.3f}"
            resp['Timings']['connect'] = f"{c.getinfo(pycurl.CONNECT_TIME):.3f}"
            resp['Timings']['pretransfer'] = f"{c.getinfo(pycurl.PRETRANSFER_TIME):.3f}"
            resp['Timings']['redirect'] = f"{c.getinfo(pycurl.REDIRECT_TIME):.3f}"
            resp['Timings']['starttransfer'] = f"{c.getinfo(pycurl.STARTTRANSFER_TIME):.3f}"

            results.append(resp)
            cnt -= 1
        return results

def match_dicts(pdict, vdict):
    """
    pdict is a patterns dict and vdict is a values dict
    Match dict of RE's with dict of the compatible structure
    Iterates through pattern dict .
    If key from pdict ain't present in vdict, matching is obviously failed
    If key is present in both, but not str or dict in both — match is failed
    If key is present in both and type is str, than bool(re.match)
    If key is present in both and type is dict, than recurse

    If some match didn't succeeded, return False immediately
    When all is over, return `isMatched` flag
    """
    isMatched: False
    for k in pdict:
        if not k in vdict:
            isMatched = False
        elif type(pdict[k]) is str and type(vdict[k]) is str:
            isMatched = bool(re.match(pdict[k], vdict[k]))
        elif type(pdict[k] is dict and type(vdict[k] is dict)):
            isMatched = match_dicts(pdict[k], vdict[k])
        else:
            isMatched = False
        if not isMatched:
            pdbg('Matching failed:')
            pdbg({
                'key': k,
                'pattern': pdict[k] if k in pdict else pdict,
                'with': vdict[k] if k in vdict else vdict,
                }, desc='Cannot match:')
            break;
    return isMatched

def check_expects(expects, results):
    for exp in expects:
        # pdbg(exp, flag=True)
        count = exp['count']
        matched = 0
        isMatched = False
        # assign to temporary variable to not lose original
        n_results = results
        # Trick to use boolean as selector
        m_results = {False: [], True: []}
        while n_results and len(m_results[True]) < count:
            [res, *n_results] = n_results
            m = match_dicts(exp['matches'], res)
            m_results[m].append(res)
            # pdbg(m, flag=True)
        matched = len(m_results[True])
        isMatched = matched == count
        exp['matched'] = matched
        results = m_results[False] + n_results
        pdbg(results, desc='results')
        pdbg(exp)
    return results

### Assuming utf-8 encoding as de-facto standard on web.
### Will decode response and save headers, status and, conditionally, body
def parse_response(headersRaw, bodyRaw):
    headers = headersRaw.getvalue().decode('utf-8')
    resp = {}
    ### Headers ﬁrst line is always status line
    [statusLine, *rest] = [l.strip() for l in  headers.splitlines() if l]
    ### Select Protocol, Response Code and Status string from it
    [Proto, Code, Status] = statusLine.split(maxsplit=2)
    resp['Code'] = Code
    resp['Proto'] = Proto
    resp['Status'] = Status
    ### Attach body
    resp['Body'] = bodyRaw.getvalue().decode('utf-8')
    resp['BodyLen'] = len(resp['Body'])
    resp['Headers'] = {}
    ### Fill Headers
    for l in rest:
        xtend_dict_val(l.split(': ', maxsplit=1), resp['Headers'])   
    return resp

import yaml
import sys, os

debug_flag = False

def set_dflag(flag=False):
	global debug_flag
	debug_flag = flag

### pretty object output
###### Accepts any yaml-dumpable object and optional description
###### Output will be prepended with function name, callee line and module file basename
def pdbg(obj, desc=None, flag=None):
    if not debug_flag and not flag:
    	return
    k = sys._getframe(1)
    desc = desc + '⇒\n' if type(desc) is str else ''
    bname = os.path.basename(k.f_code.co_filename)
    print(f"From: {bname}@{k.f_lineno}⇒{k.f_code.co_name}\n{desc}{yaml.dump(obj)}")

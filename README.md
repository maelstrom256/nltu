[[_TOC_]]

# Nginx Limits Testing Utility (nltu)
This utility serves for testing nginx `limit_req` and `limit_conn` directives.

It makes bursts of requests in order to reach limits and reports how much requests are made successfully and how much ain't.

### Working process
Utility imports yaml files as a config, dicting them on one-per-key basis into internal config object. It supports as one file per config object as import additional configs from specified directory in the config object. Also, import from directory is supported.

All additional configs will be imported in config object as an dict elements, with filename as a key.

After import's succession utility creates `Curler` object that accepts partial config object in `configs` dict as input, fills test suit, squashes test suit data with defaults options, runs tests via `Curl` of libcurl, parses binary results into strings and responce values, adds responce metrics, checks for expectations matches and filter out matched queries, prepares and outputs the formatted results with `Repoter` object using `PLTables` fork of `PrettyTables` and `ansi` module to colorize output.

It's possible to supply several configs at once as an commandline args.

### Configuration yamls
Mainly, configuration consists of folder, containing file `settings.yaml` and one or more folders with yamls, one per specific test case.

It's also possible integrate all tests in one yaml or place part of tests in `settings.yaml` and others in specified folder or combine them all in a yaml file with custom name instead of folder with strict `settings.yaml`, while one preserves object structure.

##### Base `settings.yaml` structure
Content of `settings.yaml` is a subject to import to dict with a key named as it's folder name for default `settings.yaml` or it's own filename otherwise.

Actually, utility takes into account three basic keys for now:
* `defaults`: various default settings, will be merged with same named key in every single test suit
* `test-suits`: (empty) dict of individual test suits
* `test-suits-dir`: directory name to load additional test suits from

##### Base test suit structure
It's another yaml object to import, and consists of these keys:
* `defaults`: default options for very test in a suit, will be megred via `deepcopy` with suit's defaults
* `tests`: dict, containing individual tests in case

##### `defaults` structure
It can wrap in any usable test option (see below).

##### Single test options
* `targets` object to retrieve target to run queries against. Usually contained in `settings.yaml`, but in-suit specification may extend or override these values in particular tests
* `target` is a selector for `targets`, to retrieve hostname and substitute dns settings (useful to run tests through internal connection with hostname specified). Usually they provided in `defaults` in a root of config object (file, specified by command line or `settings.yaml` otherwise), but also they can be provided in every test suit and even in *every* test personally
* `server-type` helps to select server from `targets`
* `proto` specifies a protocol: \[`http`|`https`\]
* `path` just sets up the path for url. May include url params also
* `repeat` a number to repeat this request in row
* `curl_options` dict with other curl options, like:
  ```yaml
  curl_options:
  - SSL_VERIFYPEER: 0   
  - SSL_VERIFYHOST: 0   
  ```
  Full list of options available in `pycurl.Curl`

###### Non-implemented options
* `body_limit` truncate the body to that size after successful match
* `save_cookies` save cookies after each request
* `reset_cookies` clean up cookies before making request
* `same_conn` don't close `Curl` object to keep connection

### Sample config
* `settings.yaml`
  ```yaml
  defaults:
    targets:           # targets for tests
      Prod:            # just label, referenced by tests section
        dns:           # dns substitution, curl-like format
          - 'server.local:443:192.168.42.42'
          - 'api.server.local:443:192.168.42.42'
          - 'server.local:80:192.168.42.42'
          - 'api.server.local:80:192.168.42.42'
        hostnames:                       # contains selectors for `server-type`
          api: 'api.server.local'
          web: 'server.local'
      Test:
        dns:
          - 'server.local:443:192.168.42.142'
          - 'api.server.local:443:192.168.42.142'
          - 'server.local:80:192.168.42.142'
          - 'api.server.local:80:192.168.42.142'
        hostnames:
          api: 'api.server.local'
          web: 'server.local'
  test-suits-dir: 'tests'
  test-suits: {}
  ```
* `tests/http-redirect.yaml`
  ```yaml
  defaults:               # will apply for every test. override by set in the test explicitly
    proto: 'https'        # protocol
    body_limit: 1024      # limit for response body, all above will be discarded after matching
    save_cookies: false   # save cookies from 'Set-Cookie' header
    reset_cookies: true  # strip out saved cookies
    same_conn: true       # use same connection, not initialize new one for every request
    target: 'Test'      # select target (dns and hostname) from ones defined in settings.yaml
    curl_options:         # to apply via `getattr(pycurl, 'OPTION')`
      SSL_VERIFYPEER: 0   # these two options together behaves like
      SSL_VERIFYHOST: 0   # -k (--insecure) curl option
  tests:
    http-redirect:        # just a label
      proto: 'http'
      server-type: 'web'  # select server from target
      path: '/'           # request path
      repeat: 3           # repeat request in row
      expects:            # expectations, as parsed from pycurl
      - name: 'Success'   # just label again
        count: 2          # count. If it bigger, than `repeat`, test will never match
        matches:          # matches list: just RE's
          Code: '^301$'   # desired response code `Resp['Code']`
          Headers:        # `Resp['Headers']`, `Resp['Headers']['Location']` etc
            Content-Type: '^(.*)/html(.*)$'
            Location: '^https(.*)$'
      - name: 'Must fail'
        count: 1
        matches:
          Code: '^302$'
          Headers:
            Content-Type: '^(.*)/html(.*)$'
            Location: '^https(.*)$'
  ```
